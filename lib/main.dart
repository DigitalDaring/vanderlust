import 'dart:async';
import 'package:flutter/material.dart';
import 'package:vanderlust/models/models.dart';
import 'package:vanderlust/components/extended-text-widget.dart';
import 'package:camera/camera.dart';
import 'package:vanderlust/pages/new-event-page.dart';
import 'package:vanderlust/services/persistence-service.dart';

List<CameraDescription> cameras;

Future<Null> main() async {
  cameras = await availableCameras();
  runApp(new MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.

  static const _blackPrimaryValue = 0xFF333333;

  static const MaterialColor black = const MaterialColor(
    _blackPrimaryValue,
    const <int, Color>{
      50: const Color(0xFFe0e0e0),
      100: const Color(0xFFb3b3b3),
      200: const Color(0xFF808080),
      300: const Color(0xFF4d4d4d),
      400: const Color(0xFF262626),
      500: const Color(_blackPrimaryValue),
      600: const Color(0xFF000000),
      700: const Color(0xFF000000),
      800: const Color(0xFF000000),
      900: const Color(0xFF000000),
    },
  );

  @override
  Widget build(BuildContext context) {

    //WipeData();
    return new MaterialApp(
      title: 'Vanderlust',
      theme: new ThemeData(
        primarySwatch: black,
      ),
      home: new MasterPage(
          childView: new JourneyPage(), title: 'Vanderlust Journey'),
    );
  }
}

class MasterPage extends StatelessWidget {
  MasterPage({this.childView, this.title}) : super();
  final Widget childView;
  final String title;

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(title: new Text(this.title)),
        body: new Center(child: childView));
  }
}

class DetailsText extends StatefulWidget {
  String PrimaryText;
  String SecondaryText;

  DetailsText({this.PrimaryText, this.SecondaryText});

  @override
  _DetailsTextState createState() => new _DetailsTextState(
      PrimaryText: PrimaryText, SecondaryText: SecondaryText);
}

class _DetailsTextState extends State<DetailsText> {
  String PrimaryText;
  String SecondaryText;

  _DetailsTextState({this.PrimaryText, this.SecondaryText});

  final _boldText = new TextStyle(fontWeight: FontWeight.bold);

  final _lightText = new TextStyle(color: Colors.grey[500]);

  @override
  Widget build(BuildContext context) {
    return new Row(children: [
      new Expanded(
          child:
              new Column(crossAxisAlignment: CrossAxisAlignment.end, children: [
        new Container(
            padding: const EdgeInsets.only(bottom: 8.0),
            child: new Text(PrimaryText, style: _boldText)),
        new Text(SecondaryText, style: _lightText)
      ]))
    ]);
  }
}

class JourneyPage extends StatefulWidget {
  @override
  _JourneyPageState createState() => new _JourneyPageState();
}

class _JourneyPageState extends State<JourneyPage> {
  final _biggerFont = const TextStyle(fontSize: 18.0);
  List<JourneyStop> _currentAdventure = <JourneyStop>[];

  //'https://picsum.photos/500/300'

  Widget _buildRow(JourneyStop thisPlace) {

    var imageFile = ImageFromPath(thisPlace.image);

    return new Card(
        child: new Column(children: [
      new Image.file(imageFile,
          height: 240.0, fit: BoxFit.cover),
      new Container(
          padding: const EdgeInsets.all(10.0),
          child: new Column(children: [
            DetailsText(
                PrimaryText: thisPlace.place, SecondaryText: thisPlace.country),
            ExtendedText(Content: thisPlace.adventure)
          ]))
    ]));
  }

  Widget _buildJourneyList() {
    return new ListView.builder(
        padding: const EdgeInsets.all(16.0),
        shrinkWrap: true,
        itemBuilder: (context, i) {
          if (i == 0) {
            return new Card(
                color: Colors.lightBlueAccent,
                child: new FlatButton(
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => NewEventPage()),
                      );
                    },
                    child: new Text("New Location / Event",
                        style: new TextStyle(color: Colors.white))));
          }

          if (i.isEven) {
            return new Divider();
          }

          final index = (i - 1) ~/ 2;

          if (index < _currentAdventure.length) {
            return _buildRow(_currentAdventure[index]);
          } else {
            return null;
          }
        });
  }

  @override
  Widget build(BuildContext context) {
    var futureBuilder = new FutureBuilder(
      future: GetJourney(),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.none:
            return new Text('OH NOES! NO CONNECTION!');
          case ConnectionState.waiting:
            return new Text('Waiting for some data!');
          default:
            if (snapshot.hasError)
              return new Text('Something went very wrong: $snapshot.error');
            else
              _currentAdventure = snapshot.data;
            return _buildJourneyList();
        }
      },
    );

    return futureBuilder;
  }
}


