import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:async';
import 'package:path_provider/path_provider.dart';
import 'dart:io';
import 'dart:convert';
import 'package:vanderlust/services/persistence-service.dart';

class NewEventViewModel {
  String city;
  String country;
  String description;
}

NewEventViewModel _localState = new NewEventViewModel();

class NewEventPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Oooooh, What's Going On?"),
        ),
        body: new NewEventComponent()
    );
  }
}

class EventEntryForm extends StatefulWidget {

  @override
  _EventEntryFormState createState() => new _EventEntryFormState();

}

class _EventEntryFormState extends State<EventEntryForm> {


  @override
  void initState() {
    super.initState();
    setState(() {
      _localState = new NewEventViewModel();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        TextField(
          decoration: new InputDecoration(
            icon: new Icon(Icons.place),
            helperText: 'Country'
          ),
          onChanged: (text) { _localState.country = text; },
        ),
        TextField(
          decoration: new InputDecoration(
            icon: new Icon(Icons.location_city),
            helperText: 'City'
          ),
          onChanged: (text) { _localState.city = text; },
        ),
        TextField(
          decoration: new InputDecoration(
            icon: new Icon(Icons.description),
            helperText: 'What your adventure was!'
          ),
          maxLines: 5,
          keyboardType: TextInputType.multiline,
          onChanged: (text) { _localState.description = text; },
        )
      ]
    );
  }
}

class NewEventComponent extends StatefulWidget {
  @override
  _NewEventComponentState createState() => new _NewEventComponentState();
}

class _NewEventComponentState extends State<NewEventComponent> {
  @override
  void initState() {
    super.initState();
    setState(() {

    });

  }

  @override
  void dispose() {
    super.dispose();
  }

  String timestamp() => new DateTime.now().millisecondsSinceEpoch.toString();

  File _image;

  Future getImage() async {

    var image = await ImagePicker.pickImage(source: ImageSource.camera);

    setState(() {
      _image = image;
    });
  }

  Future loadExistingImage() async {
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);
    setState(() {
      _image = image;
    });
  }

  void _showSnackBar(String text) {
    Scaffold.of(context).showSnackBar(
      new SnackBar(
          content: new Text(text)
      )
    );
  }


  @override
  Widget build(BuildContext context) {

    bool _validateFields() {
      bool valid = true;
      String errorMessage = '';

      if(_localState.description == null || _localState.description.isEmpty){
        valid = false;
        errorMessage = 'Please enter a description!';
      }

      if(_localState.city == null || _localState.city.isEmpty){
        valid = false;
        errorMessage = 'Please enter a city!';
      }

      if(_localState.country == null || _localState.country.isEmpty){
        valid = false;
        errorMessage = 'Please enter a country!';
      }

      if(!valid){
        _showSnackBar(errorMessage);
      }

      return valid;
    }

    return ListView(
        children:
        [
          Card(
            child:  _image == null ?
            new Container(
              padding: const EdgeInsets.all(30.0),
              child: Text('Please take a picture, so we can see your adventure!')
            ) :
            new Image.file(_image, height: 360.0, fit: BoxFit.cover)
          ),
          Card(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: [
                Row(
                  children: [
                    new Expanded(
                      child: IconButton(
                        iconSize: 60.0,
                        icon: new Icon(Icons.camera_alt, color: Colors.lightBlueAccent,),
                        onPressed: () {
                          getImage();
                        },
                        padding: const EdgeInsets.all(10.0),
                      ),
                    ),
                    new Expanded(
                      child: IconButton(
                        iconSize: 60.0,
                        icon: new Icon(Icons.image, color: Colors.lightBlueAccent,),
                        onPressed: () {
                          loadExistingImage();
                        },
                        padding: const EdgeInsets.all(10.0),
                      )
                    ),
                  ]
                ),
                new EventEntryForm(),
                new Container(
                  margin: const EdgeInsets.only(top: 20.0, bottom: 20.0),
                  child:
                    _image == null ?
                    new Center() :
                    new FlatButton(
                        padding: const EdgeInsets.all(10.0),
                        color: Colors.lightBlueAccent,
                        onPressed: () {
                          if(_validateFields()){
                            _addToJourney(
                                _image,
                                _localState.country,
                                _localState.city,
                                _localState.description
                            );
                          }
                        },
                        child: Text(
                          'Save this Adventure!',
                          style: new TextStyle(color: Colors.white),
                        )
                    )
                  ,
                )

              ]
            )
          )
        ]
    );
  }

  Future _addToJourney(File image, String country, String city, String description) async {
    await AddToJourney(image, country, city, description);
    Navigator.pop(context);
  }

}