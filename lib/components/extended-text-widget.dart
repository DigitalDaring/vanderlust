import 'package:flutter/material.dart';

class ExtendedText extends StatefulWidget {

  String Content;

  ExtendedText({this.Content});

  @override
  _ExtendedTextState createState() =>
      new _ExtendedTextState(Content: Content);
}

class _ExtendedTextState extends State<ExtendedText> {
  String Content;

  _ExtendedTextState({this.Content});

  @override
  Widget build(BuildContext context) {
    return new Container(
        padding: const EdgeInsets.all(8.0),
        child: new Text(
          Content,
          softWrap: true,
          textAlign: TextAlign.left,
        )
    );
  }
}