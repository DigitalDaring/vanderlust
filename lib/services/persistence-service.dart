import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:path_provider/path_provider.dart';
import 'dart:io';
import 'dart:async';
import 'package:vanderlust/models/models.dart';


Future<String> get _localPath async {
  final directory = await getApplicationDocumentsDirectory();

  return directory.path;
}

Future<File> get _localFile async {
  final path = await _localPath;
  return File('$path/journey.json');
}

File ImageFromPath(String path) {
  return File(path);
}

void WipeData() async {
  var dataFile = await _localFile;
  var jsonString = '{"stops": []}';
  await dataFile.writeAsString(jsonString);
}

Future<String> _getStoredJson() async {
  var dataFile = await _localFile;

  var jsonString = '{}';
  try {
    jsonString = await dataFile.readAsString();
  } catch (ex) {
    jsonString = '{"stops": []}';
    await dataFile.writeAsString(jsonString);
  }
  return jsonString;
}

Future<List<JourneyStop>> GetJourney() async {
  List<JourneyStop> journey = new List<JourneyStop>();

  final jsonString = await _getStoredJson();
  final decodedJson = json.decode(jsonString);

  if (decodedJson != null) {
    var returnMe =
    (decodedJson['stops'] as List).map((val) => JourneyStop.fromJson(val));
    returnMe.forEach((el) => journey.add(el));
    return journey;
  } else {
    throw Exception('Failed to load data');
  }
}

Future<File> AddToJourney(File image, String country, String city, String description) async {

  var currentJourney = await GetJourney();

  var toAdd = new JourneyStop(
      id: '1',
      image: image.path,
      country: country,
      place: city,
      coordinates: 'wherever',
      adventure: description
  );

  currentJourney.insert(0, toAdd);

  var newJourney = new JourneyList();
  newJourney.stops = currentJourney;
  final toWrite = json.encode(newJourney);

  var dataFile = await _localFile;
  final result = await dataFile.writeAsString(toWrite);
  return result;

}